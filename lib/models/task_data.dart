import 'dart:collection';

import 'package:flutter/foundation.dart';

import 'task.dart';

class TaskData with ChangeNotifier {
  List<Task> _tasks = [
    Task(name: 'Buy milk'),
    Task(name: 'Buy eggs'),
    Task(name: 'Buy bread'),
  ];

  UnmodifiableListView<Task> get tasks {
    return UnmodifiableListView(_tasks);
  }

  int get taskCount {
    return _tasks.length;
  }

  void addTask(String newTaskTitle) {
    print(newTaskTitle);
    if (newTaskTitle != null) {
      final task = Task(name: newTaskTitle);
      _tasks.add(task);
      notifyListeners();
    }
  }

  void updateTask(Task task) {
    if (task != null) {
      task.toggleDone();
      notifyListeners();
    }
  }

  void deleteTask(Task task) {
    if (task != null) {
      _tasks.remove(task);
      notifyListeners();
    }
  }
}
