import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:task_list/models/task_data.dart';
import 'package:task_list/screens/tasks_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => TaskData(),
      child: MaterialApp(
        home: TasksScreen(),
      ),
    );
  }
}
